const butonEnter = document.getElementById("enter");
const numeProdus= document.getElementById("userInput");
const descriereProdus=document.getElementById("userInputDescription");
const lista= document.querySelector("ul");

//selectare
const meniu= document.querySelector("select");
const li=lista.getElementsByTagName("li");

const butonAll=document.getElementById("all");
const butonLactate=document.getElementById("lactate");

let produse=[];
let idProduse=0;

function creareElement(){

const element=document.createElement("li");
const span=document.createElement("span");
const icon=document.createElement("i");
const descriere=document.createElement("button");

let produsNou={};

icon.classList.add("fas","fa-trash");
icon.addEventListener("click", stergereElement);
span.className="shopping-item-name";
span.textContent=numeProdus.value;
//console.log(numeProdus.value);
produsNou.nume=numeProdus.value;
produsNou.descriereProd=descriereProdus.value;

let drop=meniu.options[meniu.selectedIndex].value;
console.log(drop);
produsNou.categorie=drop;

element.classList.add(drop,"all");


descriere.textContent="detalii produs";
descriere.className="detalii";
descriere.id=`item-${idProduse}`;

produsNou.id=idProduse;
idProduse++;

element.appendChild(span);
element.appendChild(icon);
element.appendChild(descriere);

lista.appendChild(element);

numeProdus.value="";
descriereProdus.value="";

produse.push(produsNou);
console.log(produse);

}
butonEnter.addEventListener("click", creareElement);

//selectare

function filtrareLactate(){
    let i;
    for(i=0;i<li.length;i++){
        if(li[i].className=="lactate all"){
            li[i].style.display="";
        }
        else{
            li[i].style.display="none";
        }
    }
}
function filtrareAll(){
    let i;
    for(i=0;i<li.length;i++){
        if(li[i].className=="lactate all"||li[i].className=="carne all"||li[i].className=="sucuri all"){
            li[i].style.display="";
        }
        else{
            li[i].style.display="none";
        }
    }
}
butonLactate.addEventListener("click",filtrareLactate);
butonAll.addEventListener("click",filtrareAll);

//stergere 

function stergereElement(e){
const d= e.target.parentElement.children[2].id.split("-");
//console.log( e.target.parentElement.children);
let id= Number(d[1]);
console.log(d);

console.log(id);

e.target.parentElement.remove();

let newProduse=[];
for(let i=0;i<produse.length;i++){
    if(produse[i].id!=id){
        newProduse.push(produse[i]);
    }
}

produse=newProduse;

console.log(produse)

}

